<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jasa;

class JasaController extends Controller
{
    public function index()
    {
        $jasas = Jasa::orderBy('created_at', 'DESC')->get(); // 2
        // CODE DIATAS SAMA DENGAN > select * from `jasas` order by `created_at` desc 
        return view('jasas.index', compact('jasas')); // 3
    }

    public function create()
    {
        return view('jasas.create');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'nama_jasa' => 'required|string|max:100',
            'harga' => 'required|integer',
            'pembayaran' => 'required|string'
        ]);

        try {
            $jasa = Jasa::create([
                'nama_jasa' => $request->nama_jasa,
                'harga' => $request->harga,
                'pembayaran' => $request->pembayaran
            ]);
            
            return redirect('/jasa')->with(['success' => '<strong>' . $jasa->title . '</strong> Data Berhasi Disimpan']);
        } catch(\Exception $e) {
            return redirect('/jasa/new')->with(['error' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $jasa = Jasa::find($id); // Query ke database untuk mengambil data dengan id yang diterima
        return view('jasas.edit', compact('jasa'));
    }

    public function update(Request $request, $id)
    {
    $jasa = Jasa::find($id); // QUERY UNTUK MENGAMBIL DATA BERDASARKAN ID
    //KEMUDIAN MENGUPDATE DATA TERSEBUT
    $jasa->update([
                'nama_jasa' => $request->nama_jasa,
                'harga' => $request->harga,
                'pembayaran' => $request->pembayaran
    ]);
    //LALU DIARAHKAN KE HALAMAN /jasa DENGAN FLASH MESSAGE SUCCESS
    return redirect('/jasa')->with(['success' => '<strong>' . $jasa->title . '</strong> Diperbaharui']);
    }

    public function destroy($id)
{
    $jasa = Jasa::find($id); //QUERY KEDATABASE UNTUK MENGAMBIL DATA BERDASARKAN ID
    $jasa->delete(); // MENGHAPUS DATA YANG ADA DIDATABASE
    return redirect('/jasa')->with(['success' => '</strong>' . $jasa->title . '</strong> Dihapus']); // DIARAHKAN KEMBALI KEHALAMAN /jasa
}
}
