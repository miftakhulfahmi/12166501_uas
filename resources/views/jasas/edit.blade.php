@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Edit Data Jasa Pengiriman</h3>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
						
                        <!-- ACTION MENGARAH KE /jasa/id -->
                        <form action="{{ url('/jasa/' . $jasa->id) }}" method="post">
                        {{ csrf_field() }}
                            <!-- KARENA METHOD YANG AKAN DIGUNAKAN ADALAH PUT -->
                            <!-- MAKA KITA PERLU MENGIRIMKAN PARAMETER DENGAN NAME _method -->
                            <!-- DAN VALUE PUT -->
                            <input type="hidden" name="_method" value="PUT" class="form-control">
                            <div class="form-group">
                                <label for="">Nama Jasa</label>
                                <input type="text" name="nama_jasa" class="form-control" value="{{ $jasa->nama_jasa }}" placeholder="Masukkan Jasa Pengiriman">
                            </div>
                            <div class="form-group">
                                <label for="">Harga</label>
                                <input type="number" name="harga" class="form-control" value="{{ $jasa->harga }}">
                            </div>
                                <div class="form-group">
                                <label for="">Pembayaran</label>
                                <textarea name="pembayaran" cols="10" rows="10" class="form-control">{{ $jasa->pembayaran }}</textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-sm">Update</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection