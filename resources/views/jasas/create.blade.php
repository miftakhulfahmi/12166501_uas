@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Data Jasa Pengiriman</h3>
                    </div>
                    <div class="card-body">
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <form action="{{ url('/jasa') }}" method="post">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Nama Jasa</label>
                                <input type="text" name="nama_jasa" class="form-control {{ $errors->has('nama_jasa') ? 'is-invalid':'' }}" placeholder="Masukkan nama jasa pengriman">
                                <p class="text-danger">{{ $errors->first('nama_jasa') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="">Harga</label>
                                <input type="number" name="harga" class="form-control {{ $errors->has('harga') ? 'is-invalid':'' }}">
                                <p class="text-danger">{{ $errors->first('harga') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="">Pembayaran</label>
                                <textarea name="pembayaran" cols="10" rows="10" class="form-control {{ $errors->has('pembayaran') ? 'is-invalid':'' }}"></textarea>
                                <p class="text-danger">{{ $errors->first('pembayaran') }}</p>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger btn-sm">Simpan</button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection